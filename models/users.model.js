const mongoose = require('mongoose');

var bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    name: {
        type: String,
        required: true,
        trim: true,
        match: /[a-zA-Z]+/
    },
    password: {
        type: String,
        required: true
    },
    permission: {
        type: String,
        default: 'user',
        required: true
    },
    organization: {
        type: String
    },
    designation: {
        type: String,
        required: false
    },
    bio: {
        type: String,
    },
    createdUsers: Array,
    createdBy: String,
    status: {
        type: Boolean,
        default: true,
        required: false
    }
}, {
        timestamps: true,
    },
    {
        strict: false,
        collection: 'users'
    },
    {
        versionKey: false
    });

UserSchema.statics.authenticate = (email, password, callback) => {
    User.findOne({ email: email })
        .exec((error, user) => {
            if (error) {
                return callback(error);
            } else if (!user) {
                var err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            } else {
                bcrypt.compare(password, user.password, (error, result) => {
                    console.log('result', result)
                    if (result === true) {
                        return callback(null, user);
                    } else {
                        return callback();
                    }
                })
            }
        });
}

// UserSchema.pre('save', (next) => {
//     var user = this;
//     bcrypt.hash(user.password, 10, function(err, hash) {
//       if (err) {
//         return next(err);
//       }
//       user.password = hash;
//       next();
//     })
//   });


var User = mongoose.model('User', UserSchema);
module.exports = User;