const express = require("express");
const router = express.Router();
const User = require('./../models/users.model');

// const mid = require("../middleware/index");

let fetchVideoInfo = require('youtube-info');
const request = require('request-promise');
var yt = require('youtube.get-video-info');
var youTubeParser = require('youtube-parser');


router.get('/test', (req, res) => {
    res.json('Test Route')
})

router.post('/youtube-metrics', (req, res) => {
    let url = req.body.YouTubeURL;
    let yuRegex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
    let videoID = url.match(yuRegex);
    if (videoID) {
        videoID = videoID[1];
    } else {
        res.json('Need a Valid YouTube URL.')
    }
    fetchVideoInfo(videoID).then((videoInfo) => {
        let channelId = videoInfo.channelId;
        let dislikesCount = videoInfo.dislikeCount;
        let datePublished = videoInfo.datePublished;
        let embedURL = videoInfo.embedURL;
        let likesCount = videoInfo.likeCount;
        let genre = videoInfo.genre;
        let duration = videoInfo.duration;
        console.log(videoInfo)
        let options = {
            method: 'GET',
            uri: `https://www.googleapis.com/youtube/v3/channels?key=AIzaSyA4egl-TXfhHERGbXwAFAf4nN4vROwfbLA&part=contentDetails,brandingSettings,contentOwnerDetails,id,invideoPromotion,localizations,snippet,statistics,status,topicDetails&auditDetails&id=${channelId}`,
        }
        request(options).then(data => {
            let metrics = JSON.parse(data);
            let items = metrics.items[0];
            let title = items.brandingSettings.channel.title;
            let keywords = items.brandingSettings.channel.keywords;
            let description = items.brandingSettings.channel.description;
            let viewCount = items.statistics.viewCount;
            let commentCount = items.statistics.commentCount;
            let subscribersCount = items.statistics.subscriberCount;
            let videoCount = items.statistics.videoCount;
            let featuredChannels = items.brandingSettings.channel.featuredChannelsTitle;
            let topicCategories = items.topicDetails.topicCategories;
            let myJSON = { title, keywords, description, subscribersCount, genre, duration, likesCount, viewCount, commentCount, videoCount, dislikesCount, featuredChannels, topicCategories, datePublished, embedURL }
            res.send(myJSON)
        })

    })
})



module.exports = router;