const express = require("express");
const router = express.Router();
const User = require('../models/users.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

router.post("/signup", (req, res, next) => {
    if (req.body.email && req.body.name && req.body.password && req.body.cpassword) {
        // confirm that user typed same password twice
        if (req.body.password !== req.body.cpassword) {
            var err = new Error("Passwords do not match.");
            err.status = 400;
            return next(err);
        }
        // hash password before saving to DB.

        bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
            if (err) {
                return res.status(400).json({ 'msg': 'Error Hashing!' })
            }
            // create object with form input
            var userData = {
                email: req.body.email,
                name: req.body.name,
                password: hashedPassword,
                permission: "user",
                createdUsers: []
            };

            // use schema's `create` method to insert document into Mongo
            User.create(userData, (error, user) => {
                if (error) {
                    return next(error);
                }

                req.session.userId = user._id;
                req.session.username = user.name;
                req.session.role = user.permission;
                req.session.createdusers = user.createdUsers;
                var d = new Date();
                // var astring =
                //     "<p>You have last Logged into this site on <span>" +
                //     d.toLocaleDateString() +
                //     "</span> at <span>" +
                //     d.toLocaleTimeString() +
                //     "</span> </p>";
                // res.cookie("lastlogin", astring, {
                //     encode: String,
                //     path: "/"
                // });
                res.json({ 'status': 'success' })
            })
        })

    } else {
        const err = new Error("All fields required.");
        err.status = 400;
        return next(err);
    }
});

router.post('/login', (req, res) => {
    console.log('req.session', req.session);
    if (req.body.email && req.body.password) {
        User.authenticate(req.body.email, req.body.password, (error, user) => {
            if (error || !user) {
                return res.status(400).json({ authorization: false });
            } else {
                const userdetails = req.session.user = {
                    name: user.email,
                    id: user._id
                };
                const token = jwt.sign(userdetails, 'ComplicatedVToken', {
                    expiresIn: "3h",
                });
                return res.json({
                    authorization: true,
                    authToken: token
                });
            }
        });
    } else {
        return res.status(400).json({
            authorization: false,
            msg: 'Both email and password are required',
        });
    }
});

// router.get('/logout', (req, res) => {
//     let user = req.session.user
//     // req.session.destroy();
//     res.status(200).json({'user':`${user}`, 'loggedOut':true})
// })


module.exports = router;