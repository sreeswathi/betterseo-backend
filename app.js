const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const MongoStore = require('connect-mongo')(session);

// middleware
const middleware = require('./middleware');

// Routers
const analytics = require('./controllers/api.analytics');
const auth = require('./controllers/api.authentication');


// const log = middleware.logger;

// Server Config
const serverConfig = require('./config/server.config');

// DB Config and Connections

const dbConnect = require('./config/db.connection')['production'];
mongoose.Promise = global.Promise;
mongoose.connect(dbConnect.connectionString,{useNewUrlParser: true },
	(err) => {
		if (err) {
			console.log(err)
		} else {
			console.log(`Connection to ${dbConnect.db} Successfull `);
		}
	});


//use sessions for tracking logins
app.use(cookieParser());
// app.use(flash());

app.use(session({
	secret: 'BetterSEOBackendSecretItis.',
	resave: true,
	saveUninitialized: false,
	store: new MongoStore({
		mongooseConnection: mongoose.connection
	})
}));

// Robots.txt Configuration

app.get('/robots.txt', (req, res) => {
	res.type('text/plain');
	res.send('User-agent: *\nDisallow: /');
});

/**
 * App Middlewares
 */

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true,
}));

// Setting view to ejs
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'assets')));

app.use((req, res, next) => {
	// Enabling CORS
	req.userip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, accessToken, contentType, Content-Type, Authorization');
	next();
});

/**
 * API Urls
 */
app.use('/api/v1', auth);
app.use('/api/v1', analytics);

app.options('*', (req, res) => {
	res.end();
});

// To show which URL is being hit in the Console/Terminal.

app.use((req, res, next) => {
	console.log('info', `${req.method} : ${req.url}`);
	next();
});

/**
 * Uncaught Exceptions and Unhandled Rejections Handler
 */
process.on('unhandledRejection', (reason, rejectedPromise) => {
	console.log('error', reason);
});

process.on('uncaughtException', (err) => {
	console.log('error', err.message, err.stack);
	process.exit(1);
});

// 404 handler
app.use((req, res, next) => {
	res.status(404).json('Not Found');
});

/**
 * Error Handler
 */
app.use((err, req, res) => {
	res.status(err.status || 500).send()
});

let port;
let serverURL = serverConfig.url;
let environment = serverConfig.environment ;

environment === "development" ? serverURL : 'https://betterseo.herokuapp.com';

if (environment == "development") {
	port = 5601
} else {
	port = process.env.PORT || 8080;
}


app.listen(port, serverConfig.url, () => {
	console.log('info', `listening at port ${port}`);
});