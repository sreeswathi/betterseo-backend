const jwt = require('jsonwebtoken');
const moment = require('moment');

// const winston = require('winston');

const secretkeys = require('../config/secrets.config');


// const logger = new(winston.Logger)({
//     transports: [
//         new(winston.transports.Console)(),
//     ]
// });

// const log = (type, message, error) => {
//     if (type === 'error') {
//         if (error) logger.log(type, String(error));
//         if (message) logger.log(type, String(message));
//     } else {
//         logger.log(type, JSON.stringify(message));
//     }
// };


const apiAuthentication = (req, res, next) => {
    const token = req.headers.authtoken;
    if(token){
        jwt.verify(token, secretkeys.secret, (err, decoded) => {
            if (err){
                res.status(403).json({
                    success: 'false',
                    message: 'Wrong Token, You dont have access.'
                })
            } else {
                req.user = decoded;
                next();
            }
        })
    } else {
        res.status(403).send({
            success: false,
            message: 'No token provided.',
          });
    }
}

// check user already logged in or not
const checkAuth = (req, res, next) => {
    if (req.session && req.session.userId) {
        return res.redirect('/');
    }
    return next();
}

// if user not logged in go to login page
const requiresLogin = (req, res, next) => {
    if (req.session && req.session.userId) {
        return next();
    } else {
    //    return res.redirect('/')
        res.render('partials/login');
    }
}


// module.exports.logger = log;
module.exports.apiAuthentication = apiAuthentication;
module.exports.checkAuth = checkAuth;
module.exports.requiresLogin = requiresLogin;

