module.exports = {
    port : 5601,
    url : "localhost",
    environment: process.env['NODE_ENV'] = 'production'
};